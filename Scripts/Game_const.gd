extends Node

const GRUPO_PUNK = "punk"
const GRUPO_NEW_WAVE = "new_wave"
const GRUPO_REGGAE = "reggae"
const GRUPO_SOUL = "soul"

const VELOCIDADE_INIMIGOS = 200

const FILEPATH = "user://scoreboard.json"


var scoreBoard = {
	"names" : {
		"first":"",
		"second":"",
		"third":""
	},
	"scores" : {
		"first":0,
		"second":0,
		"third":0
	}
}


var delaySpawn = 5 setget setDelaySpawn

var highScore = 0

func _ready():
	pass

func setDelaySpawn(delay):
	delaySpawn = delay
	pass

func save_scoreboard():
	var save_file = File.new()
	save_file.open(FILEPATH, File.WRITE)
	save_file.store_line(scoreBoard.to_json())
	save_file.close()
	load_scoreboard()

func load_scoreboard():
	var save_file = File.new()
	if not save_file.file_exists(FILEPATH):
		print("FAIO")
		return
	
	save_file.open(FILEPATH, File.READ)
	scoreBoard = {}
	scoreBoard.parse_json(save_file.get_as_text())
	print("Saved")
#	print(scoreBoard["names"]["first"])
#	return scoreBoard


func writeScore(name, score):
	
	if score > scoreBoard["scores"]["first"]:
		scoreBoard["names"]["third"] = scoreBoard["names"]["second"]
		scoreBoard["names"]["second"] = scoreBoard["names"]["first"]
		scoreBoard["names"]["first"] = name
		
		scoreBoard["scores"]["third"] = scoreBoard["scores"]["second"]
		scoreBoard["scores"]["second"] = scoreBoard["scores"]["first"]
		scoreBoard["scores"]["first"] = score
		
	elif score > scoreBoard["scores"]["second"] and score <= scoreBoard["scores"]["first"]:
		scoreBoard["names"]["third"] = scoreBoard["names"]["second"]
		scoreBoard["names"]["second"] = name
		
		scoreBoard["scores"]["third"] = scoreBoard["scores"]["second"]
		scoreBoard["scores"]["second"] = score
		
	elif score > scoreBoard["scores"]["third"]:
		scoreBoard["names"]["third"] = name
		
		scoreBoard["scores"]["third"] = score
	save_scoreboard()

func testScore(score):
	load_scoreboard()
	if score > scoreBoard["scores"]["third"]:
		return true
	else :
		return false
	

