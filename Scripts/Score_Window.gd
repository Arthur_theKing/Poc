extends Sprite



func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass
	
func printScoreboard():
	Game_const.load_scoreboard()
	var scoretxt = ""
	for key in Game_const.scoreBoard:
		for key2 in Game_const.scoreBoard[key]:
			
			if key == "names":
				scoretxt = str( Game_const.scoreBoard[key][key2], ": ",  Game_const.scoreBoard["scores"][key2], "\n", scoretxt)
	get_node("leNome").hide()
	get_node("btnSave").hide()
	get_node("lblPts").hide()
	get_node("lblPontos").hide()
	get_node("lblNome").hide()
	get_node("lblTitle").set_text("Melhores Jogadores:")
	get_node("lblScore").set_text("")
	print(scoretxt)
	get_node("lblScore").set_text(scoretxt)
	scoretxt = ""


func _on_button_aliados_released():
	show()
	printScoreboard()


func _on_btnCloseSB_released():
	hide()
