extends Area2D

#Signals
signal enableEspecial
signal health_changed
signal health2_changed
signal dead
signal add_score

#Variaveis
var score = 0
var shieldOn = false
var especialDelay = false
var tempoVivo = 0
var vida = 15
var vida2 = 15
export var max_power = 100



func _ready():
	set_process(true)
	set_process_input(true)
	get_node("AnimationPlayer").play("run2")

func _process(delta):
#	relogio(delta)
	atualizaVida()

func _input(event):
	
	if vida < 100:
		if event.is_action_pressed("click"):
			vida = vida + 2
	
	if vida2 < 100:
		if event.is_action_pressed("click2"):
			vida2 = vida2 + 2

# Signals Functions----------------------------------------------------------------------------------------------------------

func _on_Node2D_area_enter( area ):
	
	get_node("AnimationPlayer 2").play("Hit")
	
	if area.is_in_group(Game_const.GRUPO_PUNK):
		area.queue_free()
		if shieldOn != true:
			vida = vida - 8
			vida2 = vida2 - 6
	elif area.is_in_group(Game_const.GRUPO_NEW_WAVE):
		area.queue_free()
		if shieldOn != true:
			vida = vida - 7
			vida2 = vida2 - 9
	elif area.is_in_group(Game_const.GRUPO_REGGAE):
		area.queue_free()
		if shieldOn != true:
			vida = vida - 10
			vida2 = vida2 - 5
	elif area.is_in_group(Game_const.GRUPO_SOUL):
		area.queue_free()
		if shieldOn != true:
			vida = vida - 6
			vida2 = vida2 - 11


func _on_btnShield_pressed():
	shieldOn = true


func _on_btnArrows_pressed():
	pass


func _on_AnimationPlayer_2_finished():
	get_node("AnimationPlayer 2").stop(true)

func _on_Timer_timeout():
	shieldOn = false
	especialDelay = true

func _on_TimerA_timeout():
	especialDelay = true


func _on_Delay_timeout():
	print("PERSONAGEM_DALAY_IS_OVER")
	especialDelay = false

func _on_Timer_score_timeout():
	score += 1
	emit_signal("add_score", score)

# Life and Shield Functions----------------------------------------------------------------------------------------------------------

func atualizaVida():
	if shieldOn != true:
		if vida <= 0 and vida2 <= 0:
			emit_signal("dead", score)
		elif vida <= 0:
			emit_signal("dead", score)
		elif vida2 <= 0:
			emit_signal("dead", score)
#	Libera Especiais
		elif vida >= 100 and vida2 >=100:
			if especialDelay != true:
				vida = 100
				vida2 = 100
				emit_signal("enableEspecial")
		elif vida >= 100:
			if especialDelay != true:
				vida = 100
				emit_signal("enableEspecial")
		elif vida2 >= 100:
			if especialDelay != true:
				emit_signal("enableEspecial")
				vida2 = 100
	
	emit_signal("health_changed", vida)
	emit_signal("health2_changed", vida2)

# Time Related Functions----------------------------------------------------------------------------------------------------------

func relogio(delta):
	tempoVivo += delta
	score = int(tempoVivo)
	





