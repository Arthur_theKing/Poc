extends Node

var pre_caminho = preload("res://Scenes/Caminhos/Caminho1.tscn")
var pre_caminho2 = preload("res://Scenes/Caminhos/Caminho2.tscn")
var pre_caminho3 = preload("res://Scenes/Caminhos/Caminho3.tscn")
var pre_caminho4 = preload("res://Scenes/Caminhos/Caminho4.tscn")
var pre_caminho5 = preload("res://Scenes/Caminhos/Caminho5.tscn")
var pre_caminho6 = preload("res://Scenes/Caminhos/Caminho6.tscn")
var pre_caminho7 = preload("res://Scenes/Caminhos/Caminho7.tscn")
var pre_caminho8 = preload("res://Scenes/Caminhos/Caminho8.tscn")
var pre_caminho9 = preload("res://Scenes/Caminhos/Caminho9.tscn")
var pre_caminho10 = preload("res://Scenes/Caminhos/Caminho10.tscn")
var todos_caminhos = [pre_caminho, pre_caminho2, pre_caminho3, pre_caminho4, pre_caminho5, pre_caminho6, pre_caminho7, pre_caminho8, pre_caminho9, pre_caminho10]
#var maxDelay = 5 --- Arthur
var maxDelay = 3
var delay = maxDelay

func _ready():
	set_process(true)
	get_node("dynamicD").start()
	pass

func _process(delta):
	
	if delay > 0:
		delay -= delta
	else:
		delay = rand_range(0, maxDelay)
		#print(todos_inimigos[1])
		randomize()
		var caminhos = todos_caminhos[randi()%todos_caminhos.size()].instance()
		get_owner().add_child(caminhos)
	pass


func _on_dynamicD_timeout():
	if maxDelay > 0.5:
		maxDelay -= 0.1
		print("DYNAMICD_")
		print(maxDelay)
	else:
		maxDelay = maxDelay
		get_node("dynamicD").stop()
