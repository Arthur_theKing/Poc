extends Area2D

var vel = 200

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	get_parent().set_offset(get_parent().get_offset() + (vel*delta))