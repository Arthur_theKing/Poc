extends Sprite

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var dados_salvos
var punkRock = preload("res://Sprites/PunkRock.png")
var newWave = preload("res://Sprites/NewWave.png")
var reggae = preload("res://Sprites/Reggae.png")
var CAMINHO = "user://level.data"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	salvar_dados()
	carregar_dados()
	if dados_salvos["level"] == 1:
		self.set_texture(punkRock)
	elif dados_salvos["level"] == 2:
		self.set_texture(newWave)
	elif dados_salvos["level"] == 3:
		self.set_texture(reggae)
	
	pass

func salvar_dados():
	var dados = {"level" : 2}
	var arquivo = File.new()
	arquivo.open(CAMINHO, File.WRITE)
	arquivo.store_var(dados)
	pass

func carregar_dados():
	var arquivo = File.new()
	arquivo.open(CAMINHO, File.READ)
	dados_salvos = arquivo.get_var()
	pass
	