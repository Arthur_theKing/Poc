extends PathFollow2D

var pre_punk = preload("res://Scenes/Punk.tscn")
var pre_new_wave = preload("res://Scenes/New_Wave.tscn")
var pre_reggae = preload("res://Scenes/Reggae.tscn")
var pre_soul = preload("res://Scenes/Soul.tscn")

var todos_inimigos = [pre_punk, pre_new_wave, pre_reggae, pre_soul]

func _ready():
	randomize()
	var inimigos = todos_inimigos[randi()%todos_inimigos.size()].instance()
	add_child(inimigos)
	pass
