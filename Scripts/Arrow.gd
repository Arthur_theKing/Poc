extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
const VEL = 200


func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_process(true)

func _process(delta):
	if(get_pos().y >= 600):
		print("QF")
		queue_free()
	else:
		set_pos(Vector2(get_pos().x , get_pos().y + VEL * delta))

func _on_Area2D_area_enter( area ):
	if area.is_in_group(Game_const.GRUPO_PUNK):
		area.queue_free()
	elif area.is_in_group(Game_const.GRUPO_NEW_WAVE):
		area.queue_free()
	elif area.is_in_group(Game_const.GRUPO_REGGAE):
		area.queue_free()
	elif area.is_in_group(Game_const.GRUPO_SOUL):
		area.queue_free()
