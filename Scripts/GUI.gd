extends MarginContainer

onready var number_label_life = get_node("Line").get_node("Bars/LifeBar/MarginContainer/Background/Number")
onready var number_label_bar2 = get_node("Line").get_node("Bars/LifeBar 2/MarginContainer/Background/Number")
onready var bar = get_node("Line").get_node("Bars/LifeBar/TextureProgress")
onready var bar2 = get_node("Line").get_node("Bars/LifeBar 2/TextureProgress")
onready var tween = get_node("Tween")

#Pre-Loads
var arrows = preload("res://Scenes/Arrows.tscn")

#especial
var especialOn

#btnShield Nodes
onready var btnShield = get_node("Line").get_node("Gap/btnShield")
onready var animationPlayerS = btnShield.get_node("AnimationPlayer")
onready var shieldTimer = btnShield.get_node("Timer")
onready var especialDelay = get_node("Line").get_node("Gap/especialDelay")

#btnArrows Nodes
onready var btnArrows = get_node("Line").get_node("Gap/btnArrows")
onready var animationPlayerA = btnArrows.get_node("AnimationPlayer")
onready var arrowTimer = btnArrows.get_node("TimerA")

var animated_health = 0
var animated_value2 = 0
var mostraShield = true




func _ready():
	set_process(true)
	var maxHP = get_parent().get_node("Node2D").max_power
	bar.set_max(maxHP)
	bar2.set_max(maxHP)
	update_bar2(15)
	update_health(15)




func _process(delta):
	#process barra vida 1
	animaVida()
	#process barra vida 2
	animaVida2()



# Signals Functions----------------------------------------------------------------------------------------------------------


func _on_Node2D_health_changed(player_health):
	if (player_health > 100):
		player_health = 100
	update_health(player_health)

func _on_Node2D_health2_changed(player_value2):
	if (player_value2 > 100):
		player_value2 = 100
	update_bar2(player_value2)


func _on_Node2D_enableEspecial():
	print("GUI_ENABLESPECIAL")
	showButtons()


func _on_btnShield_pressed():
	if especialOn != true:
		activateShield()
		especialOn = true


func _on_btnArrows_pressed():
	if especialOn != true:
		activateArrows()
		especialOn = true
		

func _on_Timer_timeout():
	print("timer timed out")
	hideShield()
	especialOn = false
	shieldTimer.stop()
	especialDelay.start()

func _on_TimerA_timeout():
	hideArrows()
	especialOn = false
	arrowTimer.stop()
	especialDelay.start()


func _on_Delay_timeout():
	especialDelay.stop()

func _on_Node2D_add_score(score):
	get_node("Line/Gap/lblScore").set_text(str(score))
	

# Life Functions----------------------------------------------------------------------------------------------------------

func animaVida():
	var round_value = round(animated_health)
	number_label_life.set_text(str(round_value))
	bar.set_value(round_value)



func update_health(new_value):
	tween.interpolate_property(self, "animated_health", animated_health, new_value, 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	if not tween.is_active():
		tween.start()




func update_bar2(new_value):
	tween.interpolate_property(self, "animated_value2", animated_value2, new_value, 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	if not tween.is_active():
		tween.start()




func animaVida2():
	var round_value2 = round(animated_value2)
	number_label_bar2.set_text(str(round_value2))
	bar2.set_value(round_value2)


# Especial Powers Functions-----------------------------------------------------------------------------------------------


func showButtons():
	if especialOn != true:
		
		btnShield.show()
		btnArrows.show()


func activateShield():
	btnArrows.hide()
	animationPlayerS.play("anmBtnShield")
	shieldTimer.start()

func activateArrows():
	btnShield.hide()
	animationPlayerA.play("anmBtnArrows")
	get_parent().add_child(arrows.instance())
	arrowTimer.start()

func hideShield():
	btnShield.hide()
	animationPlayerS.stop(true)

func hideArrows():
	print("HIDEARROWS")
	btnArrows.hide()
	animationPlayerA.stop(true)





